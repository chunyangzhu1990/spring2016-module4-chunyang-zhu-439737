import re
import operator
import sys, os
import collections
import math


if len(sys.argv) < 2:    
    sys.exit("Error ! Missed filename! ")
    
filename = sys.argv[1]
f=open(filename,'r');
line = f.readline()


player_dict = {}
average_score = {}

while line:
    data = re.match(r"(\w+ \w+)\sbatted\s(\d+)\stimes with\s(\d)\shits",line)
    if data:
        name = data.group(1)
        bats = int(data.group(2))
        hits = int(data.group(3))
        if name:
            if name in player_dict:
                player_dict[name] = (player_dict[name][0] + hits, player_dict[name][1] + bats)
            else:
                player_dict[name] = (hits, bats)
     
    line=f.readline()
    
f.close()

for key in player_dict:
    average_score[key] = round(float(player_dict[key][0])/float(player_dict[key][1]), 3)
    
sorted_average_score=sorted(average_score.items(), key=lambda avg:avg[1], reverse=True)

for key,value in sorted_average_score :
	print key,": ",value
